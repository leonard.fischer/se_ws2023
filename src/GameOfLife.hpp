#pragma once
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <vector>

class GameOfLife
{
  public:
    // [impl->arch~konstruktor~1]
    GameOfLife(const size_t &height, const size_t &width);
    // [impl->arch~inhaltanpassbar~1]
    void setCell(const size_t &x_pos, const size_t &y_pos, const bool &value);
    // [impl->arch~schrittweise~1]
    void runStep();
    // [impl->arch~inhaltausgeben~1]
    void printBoard(std::ostream &out) const;
    // [impl->arch~countneighbours~1]
    uint8_t countNeighbours(const size_t &x_pos, const size_t &y_pos) const;

  private:
    size_t m_height;
    size_t m_width;
    std::vector<std::vector<bool>> m_board;

  
};

