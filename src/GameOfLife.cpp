#include "GameOfLife.hpp"

#include <iostream>
#include <stdexcept>

GameOfLife::GameOfLife(const size_t &height, const size_t &width)
    : m_height(height), m_width(width),
      m_board(
          height,
          std::vector<bool>(width, false, std::vector<bool>::allocator_type()),
          std::vector<bool>::allocator_type())
{
}

void GameOfLife::setCell(const size_t &x_pos, const size_t &y_pos,
                         const bool &value)
{
    if (x_pos >= m_width || y_pos > m_height)
    {
        throw std::invalid_argument("coorindate out of board");
    }
    m_board[y_pos][x_pos] = value;
}

void GameOfLife::runStep()
{
    auto tmp_board = m_board;
    for (size_t y_pos = 0; y_pos < m_height; ++y_pos)
    {
        for (size_t x_pos = 0; x_pos < m_width; ++x_pos)
        {
            // [impl->arch~rules~1]
            const auto count{countNeighbours(x_pos, y_pos)};
            const auto &cell = m_board[y_pos][x_pos];
            // std::cout << "cnt " << int(c) << " c " << int(cell) << "  xy" <<
            // x << " " << y <<std::endl;
            if (cell) // living
            {
                if (count < 2 || count > 3) // underpopulation or overpopulation
                {
                    tmp_board[y_pos][x_pos] = false;
                }
                else
                { // still alive
                    tmp_board[y_pos][x_pos] = true;
                }
            }
            else
            {                   // dead
                if (count == 3) // give birth
                {
                    tmp_board[y_pos][x_pos] = true;
                }
                else
                { // still dead
                    tmp_board[y_pos][x_pos] = false;
                }
            }
        }
    }
    m_board = tmp_board;
}

void GameOfLife::printBoard(std::ostream &out) const
{
    // [impl->arch~screenloeschen~1]
    out << "\033c"; // clear screen
    for (uint32_t y_pos = 0; y_pos < m_height; ++y_pos)
    {
        for (uint32_t x_pos = 0; x_pos < m_width; ++x_pos)
        {
            out << (m_board[y_pos][x_pos] ? '*' : ' ');
        }
        out << std::endl;
    }
}

// NOLINTBEGIN(readability-function-cognitive-complexity)
uint8_t GameOfLife::countNeighbours(const size_t &x_pos,
                                    const size_t &y_pos) const
{
    if (x_pos >= m_width || y_pos >= m_height)
    {
        throw std::invalid_argument("coorindate out of board");
    }
    uint8_t count = 0;
    if (x_pos > 0 && y_pos > 0)
    {
        count += m_board[y_pos - 1][x_pos - 1] ? 1 : 0;
    }
    if (y_pos > 0)
    {
        count += m_board[y_pos - 1][x_pos] ? 1 : 0;
    }
    if (x_pos > 0)
    {
        count += m_board[y_pos][x_pos - 1] ? 1 : 0;
    }
    if (x_pos < m_width - 1)
    {
        count += m_board[y_pos][x_pos + 1] ? 1 : 0;
    }
    if (x_pos < m_width - 1 && y_pos < m_height - 1)
    {
        count += m_board[y_pos + 1][x_pos + 1] ? 1 : 0;
    }
    if (x_pos > 0 && y_pos < m_height - 1)
    {
        count += m_board[y_pos + 1][x_pos - 1] ? 1 : 0;
    }
    if (x_pos < m_width - 1 && y_pos > 0)
    {
        count += m_board[y_pos - 1][x_pos + 1] ? 1 : 0;
    }
    if (y_pos < m_height - 1)
    {
        count += m_board[y_pos + 1][x_pos] ? 1 : 0;
    }
    return count;
}
// NOLINTEND(readability-function-cognitive-complexity)
