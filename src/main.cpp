#include <array>
#include <cassert>
#include <chrono>
#include <iostream>
#include <thread>

#include "GameOfLife.hpp"

constexpr uint8_t SIZE_X = 8;
constexpr uint8_t SIZE_Y = 8;

constexpr uint32_t WAITTIME = 500;
constexpr int32_t MAXSTEPS = 10;

int main()
{
    GameOfLife gol(SIZE_Y, SIZE_X);

    // Glider
    gol.setCell(1, 0, true);
    gol.setCell(2, 1, true);
    gol.setCell(0, 2, true);
    gol.setCell(1, 2, true);
    gol.setCell(2, 2, true);

    gol.printBoard(std::cout);
    std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    for (int i = 0; i < MAXSTEPS; i++)
    {
        gol.runStep();
        gol.printBoard(std::cout);
        std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    }
}