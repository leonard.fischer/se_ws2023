# Projekt Foo Test

Dies ist ein Projekt welches im Rahmen der Vorlesung Software Engineering durchgeführt werden soll.
Es ist so ausgelegt dass die Studierenden eine Softwareentwicklungsprozess mit modernen Methoden
erleben und so Erfahrungen sammeln.

Dabei wird folgendermaßen Vorgegangen:
* Einführung in GIT + CI/CD
* Erstellung von Anforderungen inkl. Tracing
* Erstellung einer Softwarearchitektur inkl. Tracing
* Erweiterung der CI/CD Pipeline um statische Codeanalyse
* Erstellung von Tests + CI/CD Pipeline

Für die weitergehende Dokumentation beginnen sie hier [Project Handbook](docs/projecthandbook.md)