cmake_minimum_required(VERSION 3.0.0)
project(gol VERSION 0.1.0)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Werror -pedantic")

FILE(GLOB Sources src/*.cpp)
FILE(GLOB Headers src/*.hpp)
FILE(GLOB Tests tests/*.cpp)
FILE(GLOB Markdown docs/*.md)

add_executable(gol ${Sources})

add_custom_target(trace
    COMMAND
    java -jar ${CMAKE_CURRENT_SOURCE_DIR}/openfasttrace-3.7.0.jar trace -o html ${Markdown} ${Headers} ${Sources} ${Tests} > ${CMAKE_CURRENT_SOURCE_DIR}/report.html || java -jar ${CMAKE_CURRENT_SOURCE_DIR}/openfasttrace-3.7.0.jar trace ${Markdown} ${Headers} ${Sources} ${Tests}
)


add_custom_target(tidy
    COMMAND
    clang-tidy ${Headers} ${Sources} -checks=readability-*,fuchsia-*,cppcoreguidelines-*
)

enable_testing()
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
add_executable( runUnitTests tests/tests_gol.cpp src/GameOfLife.cpp)
target_link_libraries(runUnitTests gtest gtest_main)
add_test( runUnitTests runUnitTests )

# now for coverage bits
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
if (CMAKE_COMPILER_IS_GNUCXX)
    include(CodeCoverage)
    append_coverage_compiler_flags()

    # we need to turn off optimization for non-skewed coverage reports
    set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -O0")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0")

    # Works
    setup_target_for_coverage_gcovr_xml(
            NAME TestCoverageXml
            EXECUTABLE runUnitTests
    )
    # Works
    setup_target_for_coverage_gcovr_html(
            NAME TestCoverageHtml
            EXECUTABLE runUnitTests
    )

endif ()
