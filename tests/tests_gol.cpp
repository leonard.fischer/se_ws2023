#include "gtest/gtest.h"
#include <limits.h>

#include <iostream>
#include <sstream>

#include "../src/GameOfLife.hpp"

namespace
{

TEST(GoL, NoError) {}

TEST(GoL, CreateDestroy)
{

    auto gol = new GameOfLife(2, 3);
    delete gol;
}

TEST(GoL, PrintKonsole)
{
    std::cout << "Test PrintKonsole" << std::endl;
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, true);
    std::stringstream output;
    gol.printBoard(output);
    std::string expectedOutput = "\033c   \n * \n   \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "PrintKonsole erfolgreich" << std::endl;
}

TEST(GoL, SchrittSimulation)
{
    std::cout << "Test SchrittSimulation" << std::endl;
    GameOfLife gol(3, 3);
    gol.setCell(1, 0, true);
    gol.setCell(1, 1, true);
    gol.setCell(1, 2, true);
    gol.runStep();
    std::stringstream output;
    gol.printBoard(output);
    std::string expectedOutput = "\033c   \n***\n   \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "Test SchrittSimulation erfolgreich" << std::endl;
}

TEST(GoL, SetCell1)
{
    std::cout << "Test SetCell1" << std::endl;
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, true);
    EXPECT_TRUE(gol.countNeighbours(0, 0) == 1);
    EXPECT_TRUE(gol.countNeighbours(1, 0) == 1);
    EXPECT_TRUE(gol.countNeighbours(2, 0) == 1);
    EXPECT_TRUE(gol.countNeighbours(0, 1) == 1);
    EXPECT_TRUE(gol.countNeighbours(2, 1) == 1);
    EXPECT_TRUE(gol.countNeighbours(0, 2) == 1);
    EXPECT_TRUE(gol.countNeighbours(1, 2) == 1);
    EXPECT_TRUE(gol.countNeighbours(2, 2) == 1);
    std::cout << "Test SetCell1 erfolgreich" << std::endl;
}

TEST(GoL, InhaltAnpassbar)
{
    std::cout << "Test InhaltAnpassbar" << std::endl;
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, true);
    EXPECT_TRUE(gol.countNeighbours(0, 0) == 1);
    std::cout << "Test InhaltAnpassbar erfolgreich" << std::endl;
}



TEST(GoL, ScreenLoeschen)
{
    std::cout << "Test ScreenLoeschen" << std::endl;
    GameOfLife gol(3, 3);
    std::stringstream output;
    gol.printBoard(output);
    std::string result = output.str();
    EXPECT_TRUE(result.find("\033c") != std::string::npos);
    std::cout << "Test ScreenLoeschen erfolgreich" << std::endl;
}

TEST(GoL, Rule1)
{
    std::cout << "Test Rule1" << std::endl;
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, false);

    gol.runStep();

    std::stringstream output;
    gol.printBoard(output);

    std::string expectedOutput = "\033c   \n   \n   \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "Test Rule1 erfolgreich" << std::endl;
}

TEST(GoL, Rule2)
{
    std::cout << "Test Rule2" << std::endl;
   
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, false);

    gol.setCell(0, 0, true);
    gol.setCell(0, 2, true);
    gol.setCell(2, 0, true);
    gol.runStep();

    std::stringstream output;
    gol.printBoard(output);

    std::string expectedOutput = "\033c   \n * \n   \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "Test Rule2 erfolgreich" << std::endl;
}

TEST(GoL, Rule3)
{
    std::cout << "Test Rule3" << std::endl;
  
    GameOfLife gol(3, 3);
    gol.setCell(1, 1, true);
    gol.setCell(0, 0, true);
    gol.setCell(0, 2, true);
    gol.setCell(2, 0, true);
    gol.setCell(2, 2, true);
    gol.runStep();

    std::stringstream output;
    gol.printBoard(output);

    std::string expectedOutput = "\033c * \n* *\n * \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "Test Rule3 erfolgreich" << std::endl;
}

TEST(GoL, Rule4)
{
    std::cout << "Test Rule4" << std::endl;

    GameOfLife gol(3, 3);
    gol.setCell(1, 1, true);
    gol.setCell(0, 0, true);
    gol.setCell(0, 2, true);
    gol.setCell(2, 0, true);
    gol.runStep();

    std::stringstream output;
    gol.printBoard(output);

    std::string expectedOutput = "\033c * \n** \n   \n";
    EXPECT_EQ(output.str(), expectedOutput);
    std::cout << "Test Rule4 erfolgreich" << std::endl;
}

class TestableGameOfLife : public GameOfLife
{
  public:
    using GameOfLife::GameOfLife;

    uint8_t testCountNeighbours(const size_t &x_pos, const size_t &y_pos) const
    {
        return countNeighbours(x_pos, y_pos);
    }
};

TEST(GoL, CountNeighbours)
{
    std::cout << "Test CountNeighbours" << std::endl;
    
    TestableGameOfLife gol(5, 5);
    gol.setCell(1, 1, true);
    gol.setCell(0, 0, true);
    gol.setCell(0, 1, true);
    gol.setCell(0, 2, true);
    gol.setCell(1, 0, true);
    gol.setCell(1, 2, true);
    gol.setCell(2, 0, true);
    gol.setCell(2, 1, true);
    gol.setCell(2, 2, true);

    uint8_t count = gol.testCountNeighbours(1, 1);

    EXPECT_EQ(count, 8);

    EXPECT_THROW(gol.testCountNeighbours(99, 99), std::invalid_argument);
    EXPECT_THROW(gol.testCountNeighbours(1, 99), std::invalid_argument);
    std::cout << "Test CountNeighbours erfolgreich" << std::endl;
}



} // namespace
