# Anforderungen

Diese Dokument enthält die Highlevel Anforderungen an das Game Of Life Projekt. Es beschreibt funktionale und nicht funktionale Anforderungen.

## Tool Dokumentation
Das Tracing Tool kann auf der Konsole oder über das CMake Target ausgeführt werden. Auf der Konsole ist `make trace` einzugeben.

Die Format Dokumentation findet sich hier:
* https://github.com/itsallcode/openfasttrace/blob/main/doc/user_guide.md

## Rahmenbedingungen:
### Die Codequalität muss regelmäßig geprüft werden.
`req~codequality~1`
Status: proposed

Description:
Die Qualität des Programmcodes muss überprüft werden. Dazu sind
automatisierte Maßnahmen zu ergreifen.

Needs: doc

### Die Software muss automatisiert getestet werden.
`req~tests~1`
Status: proposed

Description:
Fehler sind in Software häufig vorkommendes Problem. Entsprechend sind alle Artefakte automatisiert zu testen. Dabei ist eine vollständige Codeabdeckung zu erreichen (C0 = 100%).

Needs: doc

### Der Auftragnehmer muss dem Auftraggeber nachweisen, dass alle Anforderungen erfüllt und getestet wurden.
`req~requirements~1`
Status: proposed

Description:
Anforderungen stellen die Grundlage für das Vertragsverhältnis dar. Entsprechend ist für eine erfolgreiche Abnahme ein Nachweis zu erbringen, dass alle Anforderungen erfüllt wurden.

Needs: doc

## Highlevel Anforderungen:

### Das Spielfeld muss ein Recheck abbilden.
`req~spielfeld-rechteck~1`
Status: proposed

Description:
Das Spielfeld für das GameOfLife ist üblicherweise rechteckig.
Es begrenzt den Bereich in dem Leben auftreten kann.

Needs: arch

### Das Spielfeld muss durch Linien in Quadrate eingeteilt werden.
`req~spielfeld-in-zellen-teilen~1`
Status: proposed

Description:
Das Spielfeld für das GameOfLife muss durch horizontale und vertikale Linien in Quadrate eingteilt werden.
Diese Quadrate werden Zellen genannt, die für das Spiel lebendig oder tot sein können. Durch diese Einteilung gibt es quadratisch aussehende Zellen.

Needs: arch

### Der Nutzer muss mit einem Klick auf eine Zelle des Spielfeldes die angeklickte Zelle aktivieren können.
`req~zelle-durch-klick-aktivieren~1`
Status: proposed

Description:
Der Spieler muss mit einem Klick auf eine Zelle die Zelle aktiv werden lassen können.
Aktive Zellen gelten nach den Regeln als lebendig, inaktive Zellen als tot.
Dadurch ist es möglich, dass der Spieler verschiedene
Muster erstellt, die dann über ein Zeitintervall verschiedene Muster aus lebenden und toten Zellen ergeben

Needs: arch

### Der Nutzer muss mit einem Klick auf eine Zelle des Spielfeldes die angeklickte Zelle deaktivieren können.
`req~zelle-durch-klick-deaktivieren~1`
Status: proposed

Description:
Der Spieler muss mit einem Klick auf eine lebende Zelle, diese auch wieder "töten" können. Damit wird es möglich, dass der Spieler seine Wahl
wieder rückgängig machen oder beliebig anpassen kann.

Needs: arch

### Eine lebende Zelle muss gelb dargestellt werden, eine tote Zelle grau
`req~farbgebung-zellen~1`
Status: proposed

Description:
Dadurch das die Zelle bei lebendig werden die Farbe verändert, wird eine Unterscheidung zwischen lebenden und toten Zellen möglich. So kann der Spieler
die Veränderungen der Zellen vor Beginn und während des Spiels wahrnehmen.

Needs: arch

### Der Nutzer muss das Spiel, wenn es nicht läuft, mithilfe eines klicks auf einen Button starten können.
`req~button-starten~1`
Status: proposed

Description:
Der Spieler muss auf einen Button klicken können, der es ermöglicht das Spiel zu starten, damit beginnt ein Zeitintervall zu laufen, der dann
nach den Regeln des GameOfLife Zellen tötet, lebendig hält oder lebendig werden lässt.

Needs: arch

### Der Nutzer muss das Spiel, nachdem es gestartet wurde, mithilfe eines klicks auf einen Stop-Button stoppen können.
`req~button-stoppen~1`
Status: proposed

Description:
Der Spieler muss mit einem Klick auf einem Button das Spiel stoppen können. Dadurch hört das Zeitintervall auf zu laufen und das Spielfeld verändert sich nicht mehr. Der Spieler hat dann die Möglichkeit das Muster aus den toten und lebenden Zellen des letzten Zeitinvervalls zu sehen. 

Needs: arch

### Der Nutzer soll durch das ziehen an einer Scrollbar das Zeitintervall der Runden des Spiels verändern können.
`req~scrollbar-zeitintervall~1`
Status: proposed

Description:
Der Spieler soll eine Scrollbar haben mit dem er das Zeitintervall des Spiels verändern kann. So kann das Intervall zwischen den verschienden Generationen auf seine Bedürfnisse angepasst werden um den Verlauf der Veränderungen beobachten zu können.

Needs: arch


### Nach Ende eines Zeitintervalls müssen aktive Zellen bei 4 oder mehr angrenzenden aktiven Zellen inaktiv werden. 
`req~zelle-tot-ueberbevoelkert~1`
Status: proposed

Description:
Eine Regel des GameOfLife ist es, dass lebende Zellen, aufgrund von Überbevölkerung sterben werden,
wenn sie 4 oder mehr angrenzende lebende Zellen haben. Nach dem Zeitintervall
müssen also alle Zellen auf die dies zutrifft deaktiviert werden.

Needs: arch

### Nach Ende eines Zeitintervalls müssen alle aktive Zellen mit weniger als zwei angrenzenden aktiven Zellen deaktiviert werden.
`req~zelle-tot-einsam~1`
Status: proposed

Description:
Eine weitere Regel des GameOfLife ist, dass lebende Zellen mit weniger als zwei angrenzenden lebenden Zellen deaktiviert, also getoetet werden, da diese aufgrund von "Einsamkeit sterben". Daher müssen nach Ablauf eines Zeitintervalles alle Zellen deaktiviert werden, auf die das zutrifft.

Needs: arch

### Nach Ende eines Zeitintervalls müssen alle aktiven Zellen mit 2 oder 3 angrenzenden aktiven Zellen aktiv bleiben.
`req~zelle-bleibt-lebendig~1`
Status: proposed

Description:
Nach den Regeln von GameOfLife haben lebende Zellen mit 2 oder 3 angrenzenden lebenden Zellen alles was es braucht und bleibt in dem nächsten Intervall leben. Die Zellen, auf die das zutrifft verändern sich also nicht.

Needs: arch

### Nach Ende eines Zeitintervalls deaktivierte Zellen mit 3 angrenzenden aktiven Zellen aktiviert werden.
`req~zelle-beleben~1`
Status: proposed

Description:
Die letzte Regel des GameOfLife ist es, dass bei 3 angrenzenden lebenden Zellen alle toten Zellen lebendig werden sollen. Dementsprechend sollen alle toten Zellen auf die dies zutrifft, nach Ende eines Zeitintervall lebendig werden. 

Needs: arch

### Der Nutzer muss durch einen resetbutton den Zeitintervall jederzeit beenden und alle Zellen auf dem Spielfeld töten können
`req~button-reset~1`
Status: proposed

Description:
Der Nutzer muss einen Button haben um das Spiel komplett zu reseten. Dabei wird das Zeitintervall beendet und das ganze Spielfeld
resettet, wodurch alle Zellen sterben. Der Nutzer soll diesen Button zu jeder Zeit betätigen können.

Needs: arch

### Der Nutzer soll, nachdem er das Spiel mit einem Klick auf den Stop button pausiert hat, mit einem klick auf einen next-Button eine Generation manuell weiter springen können
`req~button-next~1`
Status: proposed

Description:
Der Spieler soll einen next-Button haben. Nachdem das Spiel durch einen Klick auf den Stop-Button pausiert wurde soll er mit dem next-button manuell zur nächsten Generation springen sollen. Generation bezeichnet hierbei die Veränderung, die nach jedem Zeitintervall auf dem Spielfeld passiert.

Needs: arch

### Der Nutzer soll jederzeit, wenn er mit gedrückter linker Maustaste auf das Spielfeld klickt und die Maus bewegt, die Sicht auf das Spielfeld verschieben können.
`req~sicht-veraendern~1`
Status: proposed

Description:
Der Nutzer soll seine Sicht auf dem Spielfeld durch klicken mit gedrückter linker Maustaste und bewegen der Maus verändern können. Dadurch kann, wenn Zellen außerhalb des Bereiches, den der Spieler einsehen kann, lebendig werden, diese leicht eingesehen werden. 

Needs: arch
