#include <array>
#include <cassert>
#include <chrono>
#include <iostream>
#include <thread>

#include "GameOfLife.hpp"

constexpr uint8_t SIZE_X = 8;
constexpr uint8_t SIZE_Y = 8;
const auto Delay_MS = std::chrono::milliseconds(500);

const int counter = 10;
int main()
{
    GameOfLife gol(SIZE_Y, SIZE_X);

    // Glider
    gol.setCell(1, 0, true);
    gol.setCell(2, 1, true);
    gol.setCell(0, 2, true);
    gol.setCell(1, 2, true);
    gol.setCell(2, 2, true);

    gol.printBoard(std::cout);
    std::this_thread::sleep_for(Delay_MS);
    for (int i = 0; i < counter; i++)
    {
        gol.runStep();
        gol.printBoard(std::cout);
        std::this_thread::sleep_for(Delay_MS);
    }
}
