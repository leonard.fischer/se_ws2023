# Software Design Specification

## Kontext

Das GameofLife ist ein 1970 entworfenes Spiel, welches aus Zellen besteht, die nach bestimmten Regeln leben, sterben oder lebendig werden. Das Spiel setzt abei auf Zyklen und ist kein aktives Gesellschafts- oder Kampfspiel sondern eher ein Geschehen, welches vom Nutzer verfolgt werden kann.

Benutzer: Der Spieler interagiert mit dem Spiel über eine grafische Benutzeroberfläche. Dieser kann die Höhe und Breite des Spielfeldes bestimmen, vor beginn des Spiels den Status der Zellen verändern, das Spiel starten, eine neue Periode starten und das Spiel beenden.




## Statische Sicht

![Alternativer Text](docs/images/ClassDiagram2.jpg)




Das Programm besitzt vier verschiedene Klassen. "Spiel", "Grid", "GameBoard" und "Zelle". Die Klasse Spiel besitzt die Attribute "grid", der Klasse "Zelle" und "running" als bool. Besitzt das bool "running" den wert true, so weiß das Programm, dass es durch den Nutzer gestartet wurde. Über die Klasse "GameBoard" soll der Spieler zu beginn die Höhe und die Breite des Spielfeldes einstellen. grid repräsentiert ein Objekt der Klasse "Grid", welche widerum ein Array von Zelle-Objekten besitzt. Jede Zelle hat also einen zugeordneten x und y wert, welche für die jeweilige Höhe und Breite der Zelle steht. So besitzt jede Zelle einen Standort und kann über die Methode "getZelle(x,y)" aufgerufen werden. Mit "setZelle(x,y)" kann der Status der Zelle am jeweiligen Standort verändert werden. Die Methode "updateGrid()" wird, nachdem der Spieler den wechsel zur nächsten Periode veranlasst hat, dafür sorgen, dass das Spielfeld nach den Regeln des GameofLife und damit die einzelnen Zellen verändert werden. Dort soll auch die Ausgabe stattfinden. In der Klasse "Zelle" existiert das Attribut "alive", welches ein bool ist und bei true angibt, dass eine Zelle lebt und bei false tot ist. Über die Methode "isAlive()" lässt sich der Status der jeweiligen Zelle abfragen. Mit der Methode "setAlive", kann eine Zelle auf den Status lebend oder tot gesetzt werden. Über die Methode "start" in der Klasse "Spiel" lässt sich das Spiel starten, mit "stop" kann das Spiel beendet und damit das Spiel komplett resettet werden und über "nextPeriod()" lässt sich aktiv zur nächsten Periode schalten.




## Dynamische Sicht

![Alternativer Text](docs/images/ActivityDiagram2.jpg)

### Zu beginn soll der Nutzer die Hoehe und die Breite des Spielfeldes einstellen
`arch~GameBoard~1`

Description: Der Spieler soll zunächst die Höhe und Breite des Spielfeldes einstellen, bevor er fortfahren kann. Dabei soll das Spielfeld ein Rechteck darstellen. Durch setDiemsion() soll die Einstellung der Höhe und Breite eingestellt werden. Da das Spielfeld ein Rechteck sein soll ist eine gleiche Hoehe und Breite ausgeschlossen.

Needs: impl, utest

covers:
-req~spielfeldbreite~1
-req~spielfeldhoehe~1
-req~spielfeld-rechteck~1

### Spieler kann vor Beginn den Status der Zellen verändern
`arch~Grid~1`

Description: Der Spieler kann bevor er Start gedrückt hat, Zellen auswählen und diese durch einen Klick beleben oder wieder töten. Dies geschieht über die Methode "getZelle" in der Klasse "Grid" und dazu einen Aufruf von "setZelle" der Klasse "Zelle". Durch die Methode "updateGrid" soll das Spielfeld aktualisiert und grafisch ausgegeben werden. Diese wird immer bei einem Periodenwechsel aufgerufen.

Needs: impl, utest

covers:
-req~feldsetzbar~1
-req~spielfeldprint~1

### Schrittweise
`arch~Spiel~1`

Description: Nach einer Periode, welche die Anwendung der 4 Regeln des GameofLife beschreibt, kann der Spieler mit einem Button den nächsten Periodenschritt einleiten. Dazu gibt es die Methode "nextPeriod".


Needs: impl, utest

covers:
-req~schrittweise~1

### Leben bei drei lebenden Nachbarzellen 
`arch~Grid~1`

Description: Während eines Periodenwechsels wird die Regel auf das Spielfeld angewendet. Dazu wird die Methode "updateGrid" verwendet.Die Regeln werden auf jede Zelle angewendet und anschließend werden die Änderung nach der Periode grafisch ausgegeben.
Needs: impl, utest

covers:
-req~becomealive~1
-req~overpopulation~1
-req~loneliness~1
-req~liveon~1




Das Programm startet mit der Einstellung der Breite und der Höhe des Spielfeldes. Diese muss der Spieler als erstes festlegen. Nachdem der Nutzer beides bestimmt hat kann er den Start Button drücken um die Simulation zu starten oder er hat die Möglichkeit auf einzelne Zellen zu klicken um diese lebendig werden zu lassen oder auch wieder zu töten. Dies kann er mit so vielen Zellen machen wie gewünscht, bis er auf den Startbutton drückt. Nachdem der Startbutton gedrückt wurde, geht das Programm das Spielfeld ab und verändert den Status der lebenden und toten Zellen, nach den vier Regeln des Game of Life. Dabei wird die Methode "updateGrid" aufgerufen, die die Regeln auf das Spielfeld anwendet, es aktualisiert und es visuell darstellt. Ist die Periode beendet kann der Spieler entweder aktiv die nächste Periode starten, wodurch die 4 Regeln erneut angewendet werden oder er kann das Spiel mit einem stopbutton beenden, wodurch das Spiel beendet und resettet wird. Das Spielfeld verschwindet dann und der Spieler kann wieder mit dem Bestimmen der Höhe und Breite beginnen um ein neues Spiel zu starten. 











