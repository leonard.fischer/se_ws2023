#include "GameOfLife.hpp"

#include <iostream>
#include <stdexcept>

GameOfLife::GameOfLife(const size_t &height, const size_t &width)
    : m_height(height), m_width(width)
    {
          m_board.reserve(height);
          for (size_t intervall = 0; intervall<height; ++intervall)
          {
            m_board.emplace_back(width, false);
          }
          }


void GameOfLife::setCell(const size_t &zellweite, const size_t &zellhoehe, const bool &value)
{
    if (zellweite >= m_width || zellhoehe > m_height)
    {
        throw std::invalid_argument("coorindate out of board");
    }
    m_board[zellhoehe][zellweite] = value;
}


void GameOfLife::runStep()
{
    auto tmp_board = m_board;
    for (size_t zellhoehe = 0; zellhoehe < m_height; ++zellhoehe)
    {
        for (size_t zellweite = 0; zellweite < m_width; ++zellweite)
        {
            // [impl->arch~rules~1]
            const auto cas{countNeighbours(zellweite, zellhoehe)};
            const auto &cell = m_board[zellhoehe][zellweite];
            // std::cout << "cnt " << int(c) << " c " << int(cell) << "  xy" <<
            // x << " " << y <<std::endl;
            if (cell) // living
            {
                if (cas < 2 || cas > 3) // underpopulation or overpopulation
                {
                    tmp_board[zellhoehe][zellweite] = false;
                }
                else
                { // still alive
                    tmp_board[zellhoehe][zellweite] = true;
                }
            }
            else
            {               // dead
                if (cas == 3) // give birth
                {
                    tmp_board[zellhoehe][zellweite] = true;
                }
                else
                { // still dead
                    tmp_board[zellhoehe][zellweite] = false;
                }
            }
        }
    }
    m_board = tmp_board;
}

void GameOfLife::printBoard(std::ostream &out) const
{
    if (&out == &std::cout) 
    {
    // [impl->arch~screenloeschen~1]
    out << "\033c"; // clear screen
    for (uint32_t zellhoehe = 0; zellhoehe < m_height; ++zellhoehe)
    {
        for (uint32_t zellweite = 0; zellweite < m_width; ++zellweite)
        {
            out << (m_board[zellhoehe][zellweite] ? '*' : ' ');
        }
        out << std::endl;
    }
    }
}

bool GameOfLife::isCellValid(const size_t &zellweite, const size_t &zellhoehe) const
{
    return (zellweite < m_width) && (zellhoehe < m_height);
}

uint8_t GameOfLife::countNeighbours(const size_t &zellweite, const size_t &zellhoehe) const
{
    if (!isCellValid(zellweite, zellhoehe))
    {
        throw std::invalid_argument("Koordinaten nicht auf dem Board");
    }

    uint8_t zelle = 0;

    for (int counter = -1; counter <= 1; ++counter)
    {
        for (int counter2 = -1; counter2 <= 1; ++counter2)
        {
            
            if (counter == 0 && counter2 == 0)
            {
                continue;
            }
            
            if (isCellValid(zellweite + counter, zellhoehe + counter2))
            {
                zelle += m_board[zellhoehe + counter2][zellweite + counter] ? 1 : 0;
            }
        }
    }

    return zelle;
}
