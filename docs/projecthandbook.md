# Projektbeschreibung

Dieses Projekt hat das Ziel das Game Of Life zu implementieren wie es von John Conway vorgeschlagen wurde. Dabei liegt das Schwerpunkt nicht auf der Entwicklung der Software sondern auf dem Softwareentwicklungsprozess.

Die Studierenden sollen alle Stufen eines Entwicklungsprozesses selbst erfahren und selbst Hand anlegen. Die Dokumentation geschieht dabei als MarkDown Dateien und in den späteren Phasen in Form von C++ Code.

## Weiterführende Pläne

[Anforderungen](requirements.md)
[Software Architektur](architekture.md)
[Configuration Manangement](configurationmanagement.md)

## Hintergründe

Technische Dokumentation:
* https://markdown.de/

Mehr zum Game Of Life findet sich hier:
* https://de.wikipedia.org/wiki/Conways_Spiel_des_Lebens
* https://www.youtube.com/watch?v=R9Plq-D1gEk
* https://playgameoflife.com/

