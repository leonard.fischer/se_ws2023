# Configuration Management Plan

## Ziel des Dokuments

Dieses Dokument gibt eine Übersicht über die Ablage von Artefakten und den Build Prozess des Projekts.
Der Lesende soll nach der Lektüre in der Lage sein die von ihm gewünschen Artefakte aufzufinden sowie in Grundzügen zu verstehen wie die Software hergestellt wird.

## Konfigurationselemente

Als Konfigurationselemente werden folgende Elemente geführt:
* Sourcecode Dateien in den Verzeichnissen `src` und `tests`
* Dokumente im Verzeichnis `docs`
* Buildprozess relevante Dokumente (`.gitlab-ci.yml`, `CMakeLists.txt`)
* VSCode Projektkonfiguration (`.vscode`, `.devcontainer`)

Alle Elemente werden in einem GIT in der Academic Cloud gespeichert und können mit VSCode direkt ausgecheckt werden.

## Arbeitsumgebung

Die Software wird mithilfe von VSCode entwickelt. Das Projektverzeichnis enthält eine Konfiguration für VS Remote Containers mit entsprechendem Docker Container. Bei öffnen des Projektverzeichnisses wird der Container automatisch erzeugt und die Projektumgebung hergestellt.

## Buildprozess

Der Buildprozess teilt sich in drei Teile:
* Anforderungsverfolgung:
  Zur Anforderungsverfolgung wird OpenFastTrace genutzt. Es führt ein Tracing auf Markdown und Source Dateien durch und stellt diese später als Report bereit.
* Softwareerstellung:
  Die Softwareerstellung geschieht mittels CMake. Diese kann auf der Console oder über das VSCode Plugin gestartet werden.
* Testen:
  Zum Testen kommt googletest zum Einsatz. Gefordert wird eine Testabdeckung von C0 = 100%. Zusätzlich wird clang-tidy zur statischen Analyse von Fehlern eingesetzt. Angewendet werden die Regelsätze 
  readability-*, fuchsia-*, cppcoreguidelines-*. Angestrebt wird ein Build ohne Incidents.

Der Buildprozess kann sowohl von Entwicklern als auch auf einem CICD System eingesetzt werden. In diesem Projekt wird auf das integierte CICD von GITLab in der Academic Cloud zurückgegriffen.

`doc~tracing~1`
Covers: 
* `req~requirements~1`
`doc~quality~1`
Covers: 
* `req~codequality~1`
`doc~tests~1`
Covers: 
* `req~tests~1`

## Releases

Es ist nicht geplant ein Release dieser Software durchzuführen.
