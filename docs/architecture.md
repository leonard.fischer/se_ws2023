# Software Design Specification

## Kontext

Das hiervorliegende Design bezieht sich ausschließlich auf die Umsetzung einer Klasse
`GameOfLife`. Es setzt die von Jon Conway vorgeschlangenen Regeln um. Die Nutzung dieser
Klasse z.B. durch ein `main`-Programm ist nicht Teil dieses Dokuments.

## Statische Sicht

![Klassendiagramm](images/gol_class.jpg)

Das Design enthält eine einzelne Klasse `GameOfLife`. Diese besitzt einen Konstruktor welcher
die Spielfeldgröße als Höhe und Bereite einer positiven Zahl als Parameter übergeben bekommt.
Zur Anpassung des Spielfelds steht die Funktion `setCell` bereit. Mit ihr kann die Startkonfiguration angepasst werden, um ein bestimmtes Szenario zu simulieren. Die Ausgabe des aktuellen Spielfelds geschieht über die Funktion `printBoard`. Ausgebgen wird ein Zeichen für jedes Feld des Spielfelds. Um eine saubere Ausrichtung zu erreichen wird der Bildschirm jedes mal vor einer Ausgabe gelöscht. Die schrittweise Simulation des Lebens wird von der Funktion `runStep` durchgeführt. Ein Aufruf dieser Funktion simuliert genau einen Zeitschritt.


### Der GoL Konstruktor muss die Größe des Spielfelds definieren.
`arch~konstruktor~1`

Needs: impl, utest
Covers: 
- req~spielfeld-rechteck~1
- req~spielfeldhoehe~1
- req~spielfeldbreite~1

### Der Inhalt des Spielfelds in der GoL Klasse muss anpassbar sein.
`arch~inhaltanpassbar~1`

Needs: impl, utest
Covers: 
- req~feldsetzbar~1

### Der Inhalt des Spielfelds muss durch die Klasse GoL auf der Console ausgeben werden.
`arch~inhaltausgeben~1`

Needs: impl, utest
Covers: 
- req~spielfeldprint~1

### Vor der Ausgabe des Spielsfelds durch die GoL Klasse auf der Console muss diese gelöscht werden.
`arch~screenloeschen~1`

Description:
Das Löschen des Bildschirms ist notwendig, da sonst keine saubere Ausrichtung der Zeichen ermöglicht wird. Dies führt zu einer unschönen
Darstellung.

Needs: impl, utest
Covers: 
- req~spielfeldprint~1

### Die GoL Klasse muss die Spielwelt schrittweise Simulieren.
`arch~schrittweise~1`

Needs: impl, utest
Covers: 
- req~schrittweise~1

## Dynamische Sicht

![Spielregeln](images/rules_ad.jpg)

Zur Umsetzung der Simulation ist es notwendig, dass diese über alle Zellen des Spielfeldes iteriert. Für jede Zelle wird dabei gezählt wie viele Nachbarn lebend sind. Die eigentlichen Spielregeln lassen sich nun wie in der Abbildung dargestellt mithilfe einer zweistufigen Entscheidung abbilden. Wurde für alle Zellen die Entscheidung über lebend/tot getroffen, so werden diese Werte ale neuen Spielfeld übernommen.

### Die GoL Klasse muss eine Funktion zur Zählung der lebenden Nachbarn einer Zelle als Hilfsfunktion bereitstehen.
`arch~countneighbours~1`

Needs: impl, utest
Covers: 
- req~becomealive~1
- req~loneliness~1
- req~liveon~1

### Die Spielregeln müssen durch die GoL Klasse mithilfe einer Entscheidungskette entsprechend dem Diagramm umgesetzt werden.
`arch~rules~1`

Needs: impl, utest
Covers: 
- req~becomealive~1
- req~loneliness~1
- req~liveon~1
- req~overpopulation~1