# Anforderungen

Diese Dokument enthält die Highlevel Anforderungen an das Game Of Life Projekt. Es beschreibt funktionale und nicht funktionale Anforderungen.

## Tool Dokumentation
Das Tracing Tool kann auf der Konsole oder über das CMake Target ausgeführt werden. Auf der Konsole ist `make trace` einzugeben.

Die Format Dokumentation findet sich hier:
* https://github.com/itsallcode/openfasttrace/blob/main/doc/user_guide.md

## Rahmenbedingungen:
### Die Codequalität muss regelmäßig geprüft werden.
`req~codequality~1`
Status: proposed

Description:
Die Qualität des Programmcodes muss überprüft werden. Dazu sind
automatisierte Maßnahmen zu ergreifen.

Needs: doc

### Die Software muss automatisiert getestet werden.
`req~tests~1`
Status: proposed

Description:
Fehler sind in Software häufig vorkommendes Problem. Entsprechend sind alle Artefakte automatisiert zu testen. Dabei ist eine vollständige Codeabdeckung zu erreichen (C0 = 100%).

Needs: doc

### Der Auftragnehmer muss dem Auftraggeber nachweisen, dass alle Anforderungen erfüllt und getestet wurden.
`req~requirements~1`
Status: proposed

Description:
Anforderungen stellen die Grundlage für das Vertragsverhältnis dar. Entsprechend ist für eine erfolgreiche Abnahme ein Nachweis zu erbringen, dass alle Anforderungen erfüllt wurden.

Needs: doc

## Highlevel Anforderungen:

### Das Spielfeld muss ein Recheck abbilden.
`req~spielfeld-rechteck~1`
Status: proposed

Description:
Das Spielfeld für das GameOfLife ist üblicherweise rechteckig.
Es begrenzt den Bereich in dem Leben auftreten kann.

Needs: arch

### Die Breite des Spielfelds muss zu Spielbeginn einstellbar sein.
`req~spielfeldbreite~1`
Status: proposed

Description:
Bevor das Spiel beginnt muss klar sein wie breit das Spielfeld ist.
Dies kann z.B. durch den Konstruktoraufruf geschenen.

Needs: arch

### Die Höhe des Spielfelds muss zu Spielbeginn einstellbar sein.
`req~spielfeldhoehe~1`
Status: proposed

Description:
Bevor das Spiel beginnt muss klar sein wie hoch das Spielfeld ist.
Dies kann z.B. durch den Konstruktoraufruf geschenen.

Needs: arch

### Die Spielfelder müssen setzbar sein.
`req~feldsetzbar~1`
Status: proposed

Description:
Die Startaufstellung muss veränderbar sein. Ohne wird ein leeres Spielfeld
ohne leben initialisiert. Dies würde jedoch zu keiner Simulation führen.

Needs: arch

### Das Spielfeld muss auf der Konsole aufgegeben werden können.
`req~spielfeldprint~1`
Status: proposed

Description:
Während der Simulation soll es möglich sein dem Spiel des Lebens zu folgen.
Dazu wird eine Ausgabe benötigt welche den aktuellen Zustand darstellt.

Needs: arch

### Die Simulation muss Schrittweise erfolgen.
`req~schrittweise~1`
Status: proposed

Description:
Um der Simulation folgen zu können, ist es notwendigt das eine Schnittstelle
geschaffen wird welche jeweils einen Simulationsschritt ausführt.
Zwischen zwei Simulationsschritten kann der Zustand beispielsweise ausgegeben werden.

Needs: arch

### Ist eine tote Zelle von genau drei Nachbarn umgeben, so muss sie im nächsten Simulationsschritt zum Leben erweckt werden.
`req~becomealive~1`
Status: proposed

Description:
Drei lebenede Nachbarzellen erzeugen genug Gesellschaft damit sich das Leben verfielfältigt.

Needs: arch

### Hat eine lebenede Zelle weniger als zwei lebende Nachbarn, so muss sie im nächsten Simulationsschritt sterben.
`req~loneliness~1`
Status: proposed

Description:
Eine Zelle mit zu wenig Gesellschaft stirbt aufgrund von Einsamkeit.

Needs: arch

### Eine Zelle mit zwei oder drei lebenden Nachbar muss im nächsten Simulationsschritt am Leben bleiben.
`req~liveon~1`
Status: proposed

Description:
Eine gesunde Umgebung resultiert in einer beständigen Population.

Needs: arch

### Eine Zelle welche mehr als drei lebende Nachbarn besitzt, muss im nächsten Simulationsschritt sterben.
`req~overpopulation~1`
Status: proposed

Description:
Bei Überbevölkerung fühlen sich lebende Wesen nicht wohl. Sie ziehen weg oder sterben wir in diesem Fall.

Needs: arch